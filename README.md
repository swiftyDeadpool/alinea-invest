#  iOS Challenge - Alinea Invest
> Candidate: Unmesh Rathod

Objective is to design mock-up provided in mail.

## ToolSet

```
IDE: Xcode
Language: Swift 5
Third-Party Libraries: Alamofire, Nimbus, Kingfisher
Code Architecture: VIPER
Backend API: Finnhub for Treading content
```

## **Modules:** 
### **AppHome**
-  Tab bar created programmatically 
- Makes a request for getting company symbols which is later used for fetching company profile

### **Explore**
-  Setup Navigation bar 
- CollectionView having CollectionView inside cell  

Used approach: 

- CustomView named 'ViewTabs' is created to show the current collection header

    - Can be made dynamic easily
    - Implicitly dependent upon collectionView

-  Used collectionView, so that it gives us option to swipe between tabs

    - Each collectionCell (eg. Themes) contains a collectionView inside
    - Having collectionView gives us flexibility of having different sorts of view layouts, Which is demonstrated in current implementation 

- With current flow, it will show Category and Themes as they're static content

    - Will load Trending content ones it fetches 


> Screenshots
- ![picture](Screenshots/p1.png)
- ![picture](Screenshots/p2.png)
- ![picture](Screenshots/p3.png)
