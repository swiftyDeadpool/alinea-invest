//
//  DataService.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation
import Alamofire

//Completion Block Defs
typealias CompletionBlock = (_ success: Bool, _ error: Error?, _ objects: [Any]) -> Void

class DataService {
    private var symbolList: [String] = []

    private static var sharedInstance: DataService?
    static var shared: DataService {
        if sharedInstance != nil {
            return sharedInstance!
        }
        sharedInstance = DataService()
        return sharedInstance!
    }

    func getSymbolList(completion: @escaping CompletionBlock) {
        let url = APIEndpoints.symbolList
        let paramaters = ["exchange": "US"]
        NetworkManager.shared.serverRequest(type: .get, url: url, parameters: paramaters, encoding: URLEncoding.queryString, headers: requestHeader(), completion: { status, error, json in

            if status == true, let symbols = json as? [[String: Any]] {
                self.symbolList = []
                for (index, symbol) in symbols.enumerated() where index < 20 {
                    self.symbolList.append(symbol["symbol"] as! String)
                }
                completion(true, nil, self.symbolList)
            } else {
                completion(false, error, [])
            }
        })
    }

    func getContent(completion: @escaping CompletionBlock) {
        let url = APIEndpoints.companyProfile

        let trendObject = ExploreContentCellObject.init(type: .trending)
        let contents: [ExploreContentCellObject] = [getCategory(), getTheme(), trendObject]

        let shuffledArray = self.symbolList.shuffled()

        Logger.log(type: .other, "Total: \(self.symbolList.count)")
        let queue = DispatchGroup()

        for (index, symbol) in shuffledArray.enumerated() where index < 5 {
            let param = ["symbol": symbol]
            Logger.log(type: .other, "Start: \(symbol)")

            queue.enter()
            NetworkManager.shared.serverRequest(type: .get, url: url, parameters: param, encoding: URLEncoding.queryString, headers: requestHeader(), completion: { status, error, json in

                Logger.log(type: .other, "Completed: \(symbol):  \(status)")
                if status, let profile = json as? [String: Any] {
                    let item = TrendingObject.init(dict: profile)
                    trendObject.items.append(item)
                }
                queue.leave()
            })
        }
        queue.notify(queue: .main, execute: {
            completion(true, nil, contents)
        })
    }

    // Using dummy data
    private func getCategory() -> ExploreContentCellObject {
        let object = ExploreContentCellObject(type: .category)

        let o1 = CellObject.init(id: "1", title: "Stocks", imgUrl: "chart")
        o1.backgroundColor = Constants.purple
        let o2 = CellObject.init(id: "2", title: "ETFs", imgUrl: "bulb")
        o2.backgroundColor = Constants.blueC
        let o3 = CellObject.init(id: "3", title: "Crypto", imgUrl: "search")
        o3.backgroundColor = Constants.yellow

        object.items = [o1, o2, o3]

        return object
    }

    private func getTheme() -> ExploreContentCellObject {
        let object = ExploreContentCellObject(type: .themes)

        let titles = [
            "Bold BioTech",
            "She runs it",
            "Crypto Central",
            "Cannabis-ness"
        ]

        for (index, title) in titles.enumerated() {
            let item = CellObject.init(id: "\(index + 1)", title: title, imgUrl: "t\(index + 1)")
            object.items.append(item as Any)
        }

        return object
    }

    private func getTrending() -> ExploreContentCellObject {
        let object = ExploreContentCellObject(type: .trending)
        let images = ["home", "homeTab", "chart", "user", "bulb", "t1", "t2", "t3", "t4"]
        object.sectionTitle = "Top Gainers"
        for i in 0...7 {
            let item = TrendingObject.init(id: "\(i)", title: "Title \(i)", imgUrl: images.randomElement()!)
            item.subtitle = "SubTitle \(i)"
            if i % 2 == 0 {
                item.gainType = .bullish
            } else {
                item.gainType = .bearish
            }

            if i == 3 {
                item.gainType = .neutral
            }

            item.changeValue = "\(i + 3)"
            object.items.append(item)
        }

        return object
    }
}

extension DataService {
    //Hardcoded for challenge purposes.
    fileprivate func requestHeader() -> [String: String] {
        return ["X-Finnhub-Token": Constants.apiKey]
    }
}
