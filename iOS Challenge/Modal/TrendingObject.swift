//
//  TrendingObject.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class TrendingObject: CellObject {
    var gainType: GainType = .neutral
    var changeValue: String = "0"

    convenience init(dict: [String: Any]) {
        self.init()

        self.id = dict["ticker"] as? String ?? ""
        self.title = dict["name"] as? String ?? ""
        self.subtitle = dict["finnhubIndustry"] as? String ?? ""
        self.imgUrl = dict["logo"] as? String ?? ""
    }

    // Cause of randomness it will give different value with each cell reuse
    // Only for demo purposes 
    func getChangeValue() -> String {
        let d1 = arc4random_uniform(6) // Random value between 0 - 5
        let d2 = arc4random_uniform(6) // Random value between 0 - 5
        return "\(d1).\(d2)" // Combining will give us value between 0 - 5.5 in double
    }

    func getGainType() -> GainType {
        return GainType.allCases.randomElement() ?? .bullish
    }

    func isObjectValid() -> Bool {
        return self.id.isStringValid && self.title.isStringValid && self.subtitle.isStringValid
    }
}
