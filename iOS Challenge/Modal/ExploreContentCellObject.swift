//
//  ExploreContentCellObject.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

class ExploreContentCellObject {
    var type: ContentType = .category
    var sectionTitle: String = ""
    var items: [Any] = []

    init(type: ContentType) {
        self.type = type
    }
}
