//
//  ItemObject.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 12/4/17.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

class ClotheObject {
    
    var brand: String?
    var category: String?
    var clean: Bool?
    var client_id: String?
    var color: String?
    var created_at: Date?
    var desc: String?
    var fabric: String?
    var filter_type: String?
    var full_picture_urls: [String] = [String]()
    var id: String?
    var length: String?
    var like: Bool?
    var name: String?
    var new: Bool?
    var notes: String?
    var outfit_ids: [String] = [String]()
    var picture_urls: [String] = [String]()
    var price: Double = 0.0
    var repair: Bool?
    var season: String?
    var sell: Bool?
    var size: String?
    var textile: String?
    var updated_at: Date?
    var firebase_conversation_key: String?
    var thumbsUpDown: String?
    
    var outfits: [OutfitObject] = [OutfitObject]()
    
    var displayName:String?{
        var name:String? = self.name?.lowercased()
        guard name != nil else {return name}
        guard brand != nil else {return name}
        name = name?.replacingOccurrences(of: brand!.lowercased(), with: "")
        name = name?.trimmingCharacters(in: .whitespacesAndNewlines)

        return name
    }
    
    convenience init(dict: NSDictionary) {
        self.init()
        
        var pictureUrls     = [String]()
        var fullPictureUrls = [String]()
        self.brand = dict.object(forKey: "brand") as? String
        self.category = dict.object(forKey: "category") as? String
        self.clean = dict.object(forKey: "clean") as? Bool ?? false
        self.client_id = dict.object(forKey: "client_id") as? String
        self.color = dict.object(forKey: "color") as? String
        self.desc = dict.object(forKey: "description") as? String
        self.fabric = dict.object(forKey: "fabric") as? String
        self.filter_type = dict.object(forKey: "filter_type") as? String
        self.id = dict.object(forKey: "id") as? String
        self.length = dict.object(forKey: "length") as? String
        self.like = dict.object(forKey: "like") as? Bool ?? false
        self.name = dict.object(forKey: "name") as? String
        self.new = dict.object(forKey: "new") as? Bool ?? false
        self.notes = dict.object(forKey: "notes") as? String
//        self.outfit_ids = dict.object(forKey: "outfit_ids") as? [String] ?? [String]()
        self.outfit_ids.append(contentsOf: [
            "f4999179-a1a0-4787-8c2f-80231a745615",
            "cbbcdcbc-74cb-4c8c-a617-3ac37a842be7",
            "b00b620d-d077-4d78-aa7a-a556f54d40d6",
            "eae10b44-4afa-422f-b7b9-c8293f219c39"
        ])
        self.repair = dict.object(forKey: "repair") as? Bool ?? false
        self.season = dict.object(forKey: "season") as? String
        self.sell = dict.object(forKey: "sell") as? Bool ?? false
        self.size = dict.object(forKey: "size") as? String
        self.textile = dict.object(forKey: "textile") as? String
        self.price        = dict.object(forKey: "price") as? Double ?? 0
        self.firebase_conversation_key = dict.object(forKey: "firebase_conversation_key") as? String

        if let url = dict.object(forKey: "picture_url") as? String, !url.isEmpty {
            pictureUrls.append(url)
        }
        if let url = dict.object(forKey: "picture2_url") as? String, !url.isEmpty {
            pictureUrls.append(url)
        }
        if let url = dict.object(forKey: "picture3_url") as? String, !url.isEmpty {
            pictureUrls.append(url)
        }
        if let url = dict.object(forKey: "picture4_url") as? String, !url.isEmpty {
            pictureUrls.append(url)
        }
        if let url = dict.object(forKey: "picture5_url") as? String, !url.isEmpty {
            pictureUrls.append(url)
        }
        if let url = dict.object(forKey: "full_picture_url") as? String, !url.isEmpty {
            fullPictureUrls.append(url)
        }
        if let url = dict.object(forKey: "full_picture2_url") as? String, !url.isEmpty {
            fullPictureUrls.append(url)
        }
        if let url = dict.object(forKey: "full_picture3_url") as? String, !url.isEmpty {
            fullPictureUrls.append(url)
        }
        if let url = dict.object(forKey: "full_picture4_url") as? String, !url.isEmpty {
            fullPictureUrls.append(url)
        }
        if let url = dict.object(forKey: "full_picture5_url") as? String, !url.isEmpty {
            fullPictureUrls.append(url)
        }
        self.picture_urls      = pictureUrls
        self.full_picture_urls = fullPictureUrls
        
    }
    
}

