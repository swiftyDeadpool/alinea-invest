//
//  SampleObject.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

public class SampleObject {
    
    var id: String!
    var name: String?
    var desc: String?
    var tags: [String] = []

    convenience init(dict: [String: Any]) {
        self.init()
        
        self.id = dict["name"] as? String ?? "" // Add Assert
        self.name = dict["name"] as? String
        self.desc = dict["description"] as? String
        if let tags = dict["tage"] as? [String] {
            self.tags = tags
        }
    }
}
