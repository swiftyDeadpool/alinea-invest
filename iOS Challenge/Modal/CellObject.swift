//
//  CellObject.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

enum ContentType {
    case category, themes, trending, header
}

enum GainType: CaseIterable {
    case neutral, bullish, bearish
}

class CellObject {
    var id: String!
    var type: ContentType = .category
    var title: String!
    var subtitle: String?
    var imgUrl: String?
    var backgroundColor: UIColor = .white

    convenience init(dict: [String: Any]) {
        self.init()
        id = dict["id"] as? String ?? ""
        self.title = dict["title"] as? String ?? ""
        self.imgUrl = dict["image_url"] as? String
        self.subtitle = dict["subtitle"] as? String
    }

    convenience init(id: String, title: String, imgUrl: String?) {
        self.init()
        self.id = id
        self.title = title
        self.imgUrl = imgUrl
    }

    func getBackgroundColor() -> UIColor {
        let colors: [UIColor] = [.red, .yellow, .cyan, .lightGray, .green, .brown]
        return colors.randomElement() ?? .yellow
    }
}
