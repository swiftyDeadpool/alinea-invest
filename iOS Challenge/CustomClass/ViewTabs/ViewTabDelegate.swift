//
//  ViewTabDelegate.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

protocol ViewTabDelegate: class {
    func tabIndexUpdated(_: Int)
}
