//
//  ViewTabs.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 14/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class ViewTabs: UIView {
    weak var delegate: ViewTabDelegate?
    var currentTabIndex = 0
    var tabs: [ViewTab] = []
    var markerView: UIView!

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
        setupTabs()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    func setupTabs() {
        // Hardcoded data for code test purpose
        let elements = ["Category", "Themes", "Trending"]

        var xPos: CGFloat = 0
        let width = self.bounds.width / 3
        for (index, title) in elements.enumerated() {
            let tab = ViewTab.init(frame: CGRect.init(x: xPos, y: 0, width: width, height: self.bounds.height))
            tab.setTitle(title, for: .normal)
            tab.index = index
            tab.setTitleColor(.darkGray, for: .normal)
            tab.titleLabel?.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
            tab.titleLabel?.textAlignment = .center
            tab.isUserInteractionEnabled = true

            switch index {
            case 0:
                tab.setTitleColor(Constants.blueC, for: .normal)
            case 1:
                // Add border on left and right side of middle view
                tab.layer.addBorder(edge: .left, color: .lightGray, thickness: 0.5, scale: 0.8)
                tab.layer.addBorder(edge: .right, color: .lightGray, thickness: 0.5, scale: 0.8)
            default:
                break
            }

            tab.addTarget(self, action: #selector(updateSelected(_:)), for: .touchUpInside)
            self.tabs.append(tab)
            xPos = tab.frame.origin.x + width

            self.addSubview(tab)
        }

        markerView = UIView.init(frame: CGRect.init(x: width * 0.15, y: self.bounds.height - 1, width: width * 0.7, height: 2))
        markerView.backgroundColor = Constants.blueC
        markerView.layer.cornerRadius = 1
        markerView.isUserInteractionEnabled = false
        self.addSubview(markerView)
    }

    public func update(index: Int) {
        guard let tab = tabs.getElementAt(index: index) else { return }
        updateSelected(tab)
    }

    @objc func updateSelected(_ item: UIButton) {
        guard let selectedItem = item as? ViewTab, selectedItem.index != currentTabIndex else { return }

        delegate?.tabIndexUpdated(selectedItem.index!)
        currentTabIndex = selectedItem.index

        for tab in self.tabs {
            if selectedItem.index == tab.index {
                tab.setTitleColor(Constants.blueC, for: .normal)
            } else {
                tab.setTitleColor(.darkGray, for: .normal)
            }
        }

        var frame = markerView.frame
        frame.origin.x = (CGFloat(selectedItem.index) * selectedItem.bounds.width) + (selectedItem.bounds.width * 0.15)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: .curveLinear, animations: {
            self.markerView.frame = frame
        }, completion: nil)
    }
}
