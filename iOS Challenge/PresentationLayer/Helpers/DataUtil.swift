//
//  DataUtil.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

class DataUtil {
    private static var sharedInstance: DataUtil?
    static var shared: DataUtil {
        if sharedInstance != nil {
            return sharedInstance!
        }
        sharedInstance = DataUtil()
        return sharedInstance!
    }

    func isNonEmptyArray(_ data: Any) -> Bool {
        if data is Array<Any>, !(data as! Array<Any>).isEmpty {
            return true
        }
        return false
    }
}
