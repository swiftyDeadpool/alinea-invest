//
//  Logger.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

struct Logger {
    // Change type to filter console log statements
    static var filters: [LoggerType] = [.none]

    enum LoggerType {
        case all, apiResponse, actionOutlet, viewSize, other, none
    }

    static func log(type: LoggerType = .all, _ items: Any) {
        if filters.contains(.all) || filters.contains(type) {
            print(items)
        }
    }
}
