//
//  HelperExtensions.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit
import Kingfisher

extension UIViewController {
    var safeTopPadding: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.top ?? 0
        }
        return 0
    }

    var safeBottomPadding: CGFloat {
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            return window?.safeAreaInsets.bottom ?? 0
        }
        return 0
    }

    var topBarHeight: CGFloat {
        return self.safeTopPadding +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
}

extension Optional where Wrapped == String {
    var isStringValid: Bool {
        guard self != nil, self! != "" else {
            return false
        }
        return true
    }
}

extension UIImageView {
    func setImageWith(_ imgUrl: String?, placeholderImage: String = Constants.imgPlaceholder) {
        guard
            imgUrl.isStringValid,
            let url = URL.init(string: imgUrl!)
            else {
                return self.image = UIImage.init(named: placeholderImage)
        }

        self.kf.indicatorType = .activity
        self.kf.setImage(
            with: url,
            placeholder: UIImage.init(named: placeholderImage),
            options: [
            .scaleFactor(UIScreen.main.scale),
            .transition(.fade(1)),
            .cacheOriginalImage
        ], progressBlock: nil, completionHandler: { result in
            switch result {
            case .success:
                Logger.log(type: .apiResponse, "ImageSetSuccessful")
            case .failure(let error):
                Logger.log(type: .apiResponse, "ImageSetFailed: \(error.errorDescription ?? "")")
                self.image = UIImage.init(named: placeholderImage)
            }
        })
    }

    func setImageColor(color: UIColor) {
        let templateImage = self.image?.withRenderingMode(.alwaysTemplate)
        self.image = templateImage
        self.tintColor = color
    }
}

extension Array {
    func getElementAt(index: Int) -> Element? {
        if 0 <= index && index < count {
            return self[index]
        } else {
            return nil
        }
    }
}

// To resize images for TabBar
extension UIImage {
    func imageResize(sizeChange: CGSize) -> UIImage {

        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen

        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        self.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))

        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
}

extension CALayer {
    // Scale indicates length of layer
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat, scale: CGFloat = 1, name: String? = nil) {
        let border = CALayer()

        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect.init(x: frame.width * (1 - scale), y: 0, width: frame.width * scale, height: thickness)

        case UIRectEdge.bottom:
            border.frame = CGRect.init(x: frame.width * (1 - scale), y: frame.height - thickness, width: frame.width  * scale, height: thickness)

        case UIRectEdge.left:
            border.frame = CGRect.init(x: 0, y: frame.height * (1 - scale), width: thickness, height: frame.height * scale)

        case UIRectEdge.right:
            border.frame = CGRect.init(x: frame.width - thickness, y: frame.height * (1 - scale), width: thickness, height: frame.height  * scale)

        case UIRectEdge.all:
            border.frame = CGRect.init(x: frame.width * (1 - scale), y: 0, width: frame.width  * scale, height: thickness)
            border.frame = CGRect.init(x: frame.width * (1 - scale), y: frame.height - thickness, width: frame.width * scale, height: thickness)
            border.frame = CGRect.init(x: 0, y: frame.height * (1 - scale), width: thickness, height: frame.height * scale)
            border.frame = CGRect.init(x: frame.width - thickness, y: frame.height * (1 - scale), width: thickness, height: frame.height * scale)

        default:
            break
        }

        border.backgroundColor = color.cgColor
        border.name = name

        for layer in self.sublayers ?? [] where (layer.name != nil && layer.name == name) {
            self.replaceSublayer(layer, with: border)
            return
        }

        addSublayer(border)
    }
}
