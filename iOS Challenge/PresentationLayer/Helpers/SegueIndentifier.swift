//
//  SegueIndentifier.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

// It is ideal to use multiple enums for this in production for readability

enum SegueIdentifier: String {
    case AppInitToAppHomeScreen
    case OutfitToDetailScreen
}
