//
//  Constants.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

enum Constants {
    static let stagingDomain = "https://finnhub.io"
    static let productionDomain = "https://finnhub.io"
    static let baseUrl = "/api/v1"
    static let apiKey = "bss27sf48v6u62sfoorg"

    static let imgPlaceholder = "wardrobe"
    static let blueC: UIColor = UIColor(red: 0.31, green: 0.31, blue: 0.78, alpha: 1.00)
    static let purple = UIColor(red: 0.64, green: 0.64, blue: 0.96, alpha: 1.00)
    static let yellow = UIColor(red: 0.97, green: 0.85, blue: 0.36, alpha: 1.00)

    static let viewTabsHeight: CGFloat = 50
    // Add nested enums if required
}

enum APIEndpoints {
    static let appExpiry = Constants.stagingDomain + "/appExpiry"
    static let symbolList = Constants.stagingDomain + Constants.baseUrl + "/stock/symbol"
    static let companyProfile = Constants.stagingDomain + Constants.baseUrl + "/stock/profile2"
}

enum APIServiceError: Error {
    case noNetwork
    case serverError
    case jsonError
    case appExpired
    case defaultError
}
