//
//  ItemDisplayCell.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class ItemDisplayCell: UICollectionViewCell {

    // MARK: -  Elements

    @IBOutlet weak var itemDescLbl: UILabel!
    @IBOutlet weak var itemImgView: UIImageView!

    // MARK: - Properties

    static var reuseId: String = "ItemCell"
    var data: CellObject!

    public func setData(item: CellObject) {
        self.data = item
        self.backgroundColor = .white
        itemDescLbl?.text = data.title
        itemImgView?.setImageWith(data.imgUrl)
    }
}

// MARK: -  Helper

extension ItemDisplayCell {

}
