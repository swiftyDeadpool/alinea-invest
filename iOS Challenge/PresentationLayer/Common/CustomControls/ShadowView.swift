//
//  ShadowView.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class ShadowView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupProperties()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupProperties()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        setupProperties()
    }

    private func setupProperties() {
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 3.0
        self.layer.shadowOpacity = 0.3
        self.layer.shadowPath = UIBezierPath.init(roundedRect: self.bounds, cornerRadius: 5).cgPath
    }
}
