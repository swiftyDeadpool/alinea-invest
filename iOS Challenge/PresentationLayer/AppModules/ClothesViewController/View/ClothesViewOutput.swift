//
//  ClothesViewOutput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

protocol ClothesViewOutput: class {
    var viewer: ClothesViewInput? { get set }
    var interactor: ClothesInteractorInput? { get set }
    var router: ClothesRouterInput? { get set }

    func viewDidLoad()
}
