//
//  ClothesViewController.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class ClothesViewController: UIViewController, ClothesViewInput {
    var output: ClothesViewOutput?

    // MARK: -  LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        ClothesAssembly.setupAssemblyFor(vc: self)
        output?.viewDidLoad()
    }

    

    // MARK: - ClothesViewInput

    func setupInitialState() {

    }
}
