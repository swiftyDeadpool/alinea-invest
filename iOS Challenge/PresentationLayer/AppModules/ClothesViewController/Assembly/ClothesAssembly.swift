//
//  ClothesAssembly.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class ClothesAssembly {
    class func setupAssemblyFor(vc: ClothesViewController) {
        let presenter: ClothesViewOutput & ClothesInteractorOutput = ClothesPresenter()

        vc.output = presenter
        vc.output?.router = ClothesRouter()
        vc.output?.viewer = vc
        vc.output?.interactor = ClothesInteractor()
        vc.output?.interactor?.output = presenter
    }
}
