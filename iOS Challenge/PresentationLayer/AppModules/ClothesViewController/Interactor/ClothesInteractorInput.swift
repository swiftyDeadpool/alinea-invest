//
//  ClothesInteractorInput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation

protocol ClothesInteractorInput: class {
    var output: ClothesInteractorOutput? { get set }

}
