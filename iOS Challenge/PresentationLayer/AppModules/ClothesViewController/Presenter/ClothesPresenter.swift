//
//  ClothesPresenter.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class ClothesPresenter: ClothesViewOutput, ClothesInteractorOutput {

    weak var viewer: ClothesViewInput?
    var interactor: ClothesInteractorInput?
    var router: ClothesRouterInput?

    // MARK: - ClothesViewOutput

    func viewDidLoad() {
        viewer?.setupInitialState()
    }
    
    // MARK: - ClothesInteractorOutput
}
