//
//  DetailAssembly.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class DetailAssembly {
    class func setupAssemblyFor(vc: DetailViewController) {
        let presenter: DetailViewOutput & DetailInteractorOutput = DetailPresenter()

        vc.output = presenter
        vc.output?.router = DetailRouter()
        vc.output?.viewer = vc
        vc.output?.interactor = DetailInteractor()
        vc.output?.interactor?.output = presenter

        vc.dataManager = DetailViewDataManager()
        vc.dataManager.view = vc
    }
}
