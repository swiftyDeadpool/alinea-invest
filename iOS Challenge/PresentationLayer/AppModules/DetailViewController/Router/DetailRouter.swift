//
//  DetailRouter.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class DetailRouter: DetailRouterInput {
    func loadDetailScreenFor(obj: ClotheObject, vc: DetailViewController) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let destinationVC = storyBoard.instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        (destinationVC as DetailViewInput).showDetailFor(object: obj)
        vc.navigationController?.pushViewController(destinationVC, animated: true)
    }
}
