//
//  DetailPresenter.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class DetailPresenter: DetailViewOutput, DetailInteractorOutput {

    weak var viewer: DetailViewInput?
    var interactor: DetailInteractorInput?
    var router: DetailRouterInput?

    // MARK: - DetailViewOutput

    func viewDidLoad() {
        interactor?.setCurrentView(status: true)
        interactor?.fetchContents()
        viewer?.setupInitialState()
    }

    func viewWillAppear() {
        interactor?.setCurrentView(status: true)
    }

    func viewWillDisappear() {
        interactor?.setCurrentView(status: false)
        viewer?.setActivityIndicator(toShow: false)
    }

    func showDetailFor(object: Any) {
        interactor?.showDetailFor(object: object)
    }

    func didClickOn(item: CellObject) {
        interactor?.didClickOn(item: item)
    }

    func getDetailObject() -> DetailCellObject {
        return interactor!.getDetailObject()
    }

    // MARK: - DetailInteractorOutput

    func dataSourceUpdated() {
        viewer?.dataSourceUpdated()
        viewer?.hideFooterView()
    }

    func configureTableFor(object: DetailCellObject) {
        viewer?.configureTableFor(object: object)
        if object.type == .outfit, object.referencedItems.isEmpty {
            viewer?.setupFooterView()
        }
    }

    func loadDetailScreenFor(obj: ClotheObject) {
        router?.loadDetailScreenFor(obj: obj, vc: self.viewer! as! DetailViewController)
    }
}
