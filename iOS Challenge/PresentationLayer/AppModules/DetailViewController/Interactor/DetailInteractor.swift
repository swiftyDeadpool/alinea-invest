//
//  DetailInteractor.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation

class DetailInteractor: DetailInteractorInput {
    var output: DetailInteractorOutput?
    var detailObject: DetailCellObject!
    var isCurrentView: Bool = false

/*
    Reason we require these 2 option as it's tricky and messy to use same model for both
    Can be solved using dictionary or inheritance
    But since there's only detail for 2 module, We can use this approach for simplicity
*/
    var outfit: OutfitObject?
    var clothe: ClotheObject?

    func showDetailFor(object: Any) {
        if object is OutfitObject {
            outfit = object as? OutfitObject

        } else if object is ClotheObject {
            clothe = object as? ClotheObject
        }
    }

    func setCurrentView(status: Bool) {
        isCurrentView = status
    }

    func fetchContents() {
        if outfit != nil {
            setupDetailFor(outfit: outfit!)
        } else if clothe != nil {
            setupDetailFor(clothe: clothe!)
        }
    }

    func getDetailObject() -> DetailCellObject {
        return detailObject
    }

    func getItems() -> [CellObject] {
        return detailObject.referencedItems
    }

    func didClickOn(item: CellObject) {
        if item.type == .clothe {
//            guard
//                let clothes = outfit?.clothes,
//                let selectedClothe = clothes.filter({$0.id ?? "" == item.id}).first
//                else { return } // Setup error flow
            let selectedClothe = outfit!.clothes.first!
            output?.loadDetailScreenFor(obj: selectedClothe)

        } else if item.type == .outfit {
            // Restrict for now
            // Results in loop
        }
    }

    private func setupDetailFor(outfit: OutfitObject) {
        detailObject = DetailCellObject.init(id: outfit.id ?? "", type: .outfit, title: outfit.name ?? "_", imgUrl: outfit.full_outfit_picture_url)
        detailObject.subtitle = outfit.category ?? "-"
        detailObject.showDislike = true
        detailObject.notes = outfit.notes

        self.output?.configureTableFor(object: detailObject)

//        DataService.shared.getItems(completion: { status, error, response in
//            guard
//                status, error == nil, response != nil,
//                let clothes = response as? [ClotheObject] else {
//                return
//            }
//
//            self.outfit?.clothes = clothes
//            let filterClothes = clothes.filter({$0.outfit_ids.contains(outfit.id ?? "")}).prefix(4)
//            self.detailObject.referencedItems = []
//            for clothe in filterClothes {
//                let item = CellObject.init(id: clothe.id ?? "", type: .clothe, title: clothe.displayName ?? "-", imgUrl: clothe.full_picture_urls.first)
//                item.subtitle = clothe.category ?? "-"
//                self.detailObject.referencedItems.append(item)
//            }
//            if self.isCurrentView {
//                self.output?.dataSourceUpdated()
//                self.output?.configureTableFor(object: self.detailObject)
//            }
//        })
    }

    private func setupDetailFor(clothe: ClotheObject) {
        detailObject = DetailCellObject.init(id: clothe.id ?? "", type: .clothe, title: clothe.name ?? "_", imgUrl: clothe.full_picture_urls.randomElement() ?? "")
        detailObject.subtitle = clothe.category ?? "-"
        detailObject.showDislike = true
        detailObject.notes = clothe.notes

        self.output?.configureTableFor(object: detailObject)
    }
}
