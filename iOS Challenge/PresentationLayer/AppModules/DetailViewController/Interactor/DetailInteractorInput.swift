//
//  DetailInteractorInput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation

protocol DetailInteractorInput: class {
    var output: DetailInteractorOutput? { get set }

    func showDetailFor(object: Any)
    func setCurrentView(status: Bool)
    func fetchContents()
    func getDetailObject() -> DetailCellObject
    func getItems() -> [CellObject]
    func didClickOn(item: CellObject)
}
