//
//  ItemDetailTableViewCell.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit
import Nimbus

class ItemDetailTableViewCell: UITableViewCell, NICell {
    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var leftView: ShadowView!
    @IBOutlet weak var leftImgView: UIImageView!
    @IBOutlet weak var leftTitleLbl: UILabel!
    @IBOutlet weak var leftSubtitleLbl: UILabel!

    @IBOutlet weak var rightView: ShadowView!
    @IBOutlet weak var rightImgView: UIImageView!
    @IBOutlet weak var rightTitleLbl: UILabel!
    @IBOutlet weak var rightSubtitleLbl: UILabel!

    static var reuseId: String = "ItemDetailTableViewCell"
    var leftData: CellObject!
    var rightData: CellObject?
    weak var delegate: ItemActionDelegate?

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        let leftTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(leftViewClicked))
        let rightTapGesture = UITapGestureRecognizer.init(target: self, action: #selector(rightViewClicked))

        leftView.addGestureRecognizer(leftTapGesture)
        rightView.addGestureRecognizer(rightTapGesture)
    }

    func shouldUpdate(with object: Any!) -> Bool {
        guard let detail = object as? DetailItemCellObject else {
            return false
        }
        self.leftData = detail.objs[0]
        self.rightData = detail.objs.getElementAt(index: 1)

        setupLeftViewUI()
        setupRightViewUI()

        return true
    }

    private func setupLeftViewUI() {
        leftView.backgroundColor = .white
        leftImgView.setImageWith(leftData.imgUrl)
        leftTitleLbl.text = leftData.title
        leftSubtitleLbl.text = leftData.subtitle ?? ""
    }

    private func setupRightViewUI() {
        rightView.backgroundColor = .white
        if rightData != nil {
            rightView.isHidden = false
            rightImgView.setImageWith(rightData!.imgUrl)
            rightTitleLbl.text = rightData!.title
            rightSubtitleLbl.text = rightData!.subtitle ?? ""

        } else {
            rightView.isHidden = true
        }
    }

    @objc private func leftViewClicked() {
        delegate?.didClickOn(item: leftData)
    }

    @objc private func rightViewClicked() {
        guard rightData != nil else { return }
        delegate?.didClickOn(item: rightData!)
    }
}
