//
//  DetailItemCellObject.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation
import Nimbus

class DetailItemCellObject: NSObject, NINibCellObject {
    var objs: [CellObject] = []

    public init(objs: [CellObject]) {
        super.init()
        self.objs = objs
    }

    class func objectWith(objs: [CellObject]) -> DetailItemCellObject {
        return DetailItemCellObject.init(objs: objs)
    }

    @objc func cellNib() -> UINib! {
        return UINib.init(nibName: NSStringFromClass(ItemDetailTableViewCell.self).components(separatedBy: ".").last!, bundle: Bundle.main)
    }

    @objc func cellNibClass() -> AnyClass! {
        return ItemDetailTableViewCell.self
    }
}


