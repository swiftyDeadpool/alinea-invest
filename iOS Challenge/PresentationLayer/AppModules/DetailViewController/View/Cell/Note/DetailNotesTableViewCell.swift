//
//  DetailNotesTableViewCell.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit
import Nimbus

class DetailNotesTableViewCell: UITableViewCell, NICell{
    @IBOutlet weak var titleLbl: UILabel!
    static var reuseId: String = "DetailNotes"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func shouldUpdate(with object: Any!) -> Bool {
        guard let detail = object as? DetailNoteCellObject else {
            return false
        }
        self.backgroundColor = .clear
        titleLbl?.isHidden = false
        titleLbl?.text = detail.title
        return true
    }
}
