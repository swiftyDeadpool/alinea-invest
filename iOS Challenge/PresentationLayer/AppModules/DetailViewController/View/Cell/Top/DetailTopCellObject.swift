//
//  DetailTopCellObject.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation
import Nimbus

class DetailTopCellObject: NSObject, NINibCellObject {
    var obj: DetailCellObject!

    public init(obj: DetailCellObject) {
        super.init()
        self.obj = obj
    }

    class func objectWith(obj: DetailCellObject) -> DetailTopCellObject {
        return DetailTopCellObject.init(obj: obj)
    }

    @objc func cellNib() -> UINib! {
        return UINib.init(nibName: NSStringFromClass(DetailTopTableViewCell.self).components(separatedBy: ".").last!, bundle: Bundle.main)
    }

    @objc func cellNibClass() -> AnyClass! {
        return DetailTopTableViewCell.self
    }
}

