//
//  DetailTopTableViewCell.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit
import Nimbus

class DetailTopTableViewCell: UITableViewCell, NICell {

    @IBOutlet weak var itemImgView: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subtitleLbl: UILabel!

    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var stackView: UIStackView!

    static var reuseId: String = "DetailTop"

    override func didMoveToSuperview() {
        super.didMoveToSuperview()

        // Setup observers
    }

    func shouldUpdate(with object: Any!) -> Bool {
        guard let detail = object as? DetailTopCellObject else {
            return false
        }
        self.backgroundColor = .clear
        itemImgView.setImageWith(detail.obj.imgUrl)
        titleLbl.text = detail.obj.title
        subtitleLbl.text = detail.obj.subtitle ?? "-"

        if !detail.obj.showDislike {
            for view in stackView.arrangedSubviews where view.tag == 2 {
                stackView.removeArrangedSubview(view)
            }
        }
        return true
    }
}
