//
//  DetailTitleCellObject.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation
import Nimbus

class DetailTitleCellObject: NSObject, NINibCellObject {
    var title: String?

    public init(title: String?) {
        super.init()
        self.title = title
    }

    class func objectWith(title: String?) -> DetailTitleCellObject {
        return DetailTitleCellObject.init(title: title)
    }

    @objc func cellNib() -> UINib! {
        return UINib.init(nibName: NSStringFromClass(DetailTitleTableViewCell.self).components(separatedBy: ".").last!, bundle: Bundle.main)
    }

    @objc func cellNibClass() -> AnyClass! {
        return DetailTitleTableViewCell.self
    }
}

