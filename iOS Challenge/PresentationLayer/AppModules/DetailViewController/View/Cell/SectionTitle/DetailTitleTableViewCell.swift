//
//  DetailTitleTableViewCell.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit
import Nimbus

class DetailTitleTableViewCell: UITableViewCell, NICell {
    @IBOutlet weak var titleLbl: UILabel!

    func shouldUpdate(with object: Any!) -> Bool {
        guard let detail = object as? DetailTitleCellObject else {
            return false
        }
        self.backgroundColor = .clear
        if detail.title.isStringValid {
            titleLbl?.isHidden = false
            titleLbl?.text = "   \(detail.title!)   "
        } else {
            titleLbl?.isHidden = true
        }
        return true
    }
}
