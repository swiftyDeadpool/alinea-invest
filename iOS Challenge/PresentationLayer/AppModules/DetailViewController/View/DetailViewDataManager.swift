//
//  DetailViewDataManager.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit
import Nimbus

class DetailViewDataManager: NSObject, UITableViewDelegate {
    weak var view: DetailViewController?
    var tableViewModel: NIMutableTableViewModel?
    var tableViewActions: NITableViewActions?
    var cellObjects: [Any] = []

    func configureDataDisplayWith(object: DetailCellObject, tableView: UITableView) {
        cellObjects = []

        if self.tableViewModel?.sections.count ?? 0 > 0 {
            _ = self.tableViewModel?.removeSection(at: 0)
        }

        let top = DetailTopCellObject.objectWith(obj: object)
        cellObjects.append(top)

        if object.notes.isStringValid {
            let noteTitle = DetailTitleCellObject.objectWith(title: "NOTES")
            let note = DetailNoteCellObject.objectWith(title: "A casual outfit for fall to winter weather. Wear this to work or on a casual day out.")
            cellObjects.append(contentsOf: [noteTitle, note])
        }

        if !object.referencedItems.isEmpty {
            let noteTitle = DetailTitleCellObject.objectWith(title: "CLOTHES INSIDE (\(object.referencedItems.count))")
            cellObjects.append(noteTitle)

            let obj1 = DetailItemCellObject.objectWith(objs: [object.referencedItems[0], object.referencedItems[1]])
            let obj2 = DetailItemCellObject.objectWith(objs: [object.referencedItems[2], object.referencedItems[3]])
            cellObjects.append(contentsOf: [obj1, obj2])
        }

        _ = self.tableViewModel?.addSection(withTitle: "")
        _ = self.tableViewModel?.addObjects(from: self.cellObjects)
    }

    // MARK: DataDisplayManager methods

    func dataSourceForTableView(tableView: UITableView) -> UITableViewDataSource {
        if tableViewModel == nil {
            self.tableViewModel = self.configureTableViewModel()

        }
        return self.tableViewModel!
    }

    func delegateForTableView(tableView: UITableView, withBaseDelegate: UITableViewDelegate?) -> UITableViewDelegate {
        if tableViewActions == nil {
            self.setupActionBlocks()
        }
        return self.tableViewActions!.forwarding(to: self )
    }


    // MARK: UITableViewDelegate methods

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let object = self.cellObjects.getElementAt(index: indexPath.item) else {
            return NICellFactory.tableView(tableView, heightForRowAt: indexPath, model: self.tableViewModel)
        }

        if object is DetailTopCellObject {
            return UIScreen.main.bounds.height * 0.8
        } else if object is DetailItemCellObject {
            return (tableView.bounds.width / 2 - 7.5) * 1.4
        } else {
            return UITableView.automaticDimension
        }
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? ItemDetailTableViewCell {
            cell.delegate = view
        }
    }

    // MARK: Private methods

    func setupActionBlocks () {
        self.tableViewActions =  NITableViewActions.init(target: self)

        let packageActionBlock: NIActionBlock = {
            _, _, _ in
            return false
        }

        self.tableViewActions?.attach(to: DetailTitleCellObject.self, tap: packageActionBlock)
    }


    private func configureTableViewModel () -> NIMutableTableViewModel {
        self.tableViewModel = NIMutableTableViewModel.init(sectionedArray: self.cellObjects, delegate: NICellFactory.self as! NITableViewModelDelegate ) as NIMutableTableViewModel
        return self.tableViewModel!
    }
}
