//
//  DetailViewController.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, DetailViewInput {
    @IBOutlet weak var tableView: UITableView!

    var dataManager: DetailViewDataManager!
    var output: DetailViewOutput?
    let activityIndicator = UIActivityIndicatorView(style: .gray)
    var isAssemblySet: Bool = false

    // MARK: -  LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        if !isAssemblySet {
            DetailAssembly.setupAssemblyFor(vc: self)
            isAssemblySet = true
        }
        output?.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output?.viewWillAppear()
        self.navigationController?.isNavigationBarHidden = false
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        output?.viewWillDisappear()
    }

    

    // MARK: - DetailViewInput

    func setupInitialState() {
        if #available(iOS 13.0, *) {
            activityIndicator.style = .large
        }
        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
        view.bringSubviewToFront(activityIndicator)
    }

    func configureTableFor(object: DetailCellObject) {
        dataManager.configureDataDisplayWith(object: object, tableView: tableView)
        tableView.dataSource = dataManager.dataSourceForTableView(tableView: tableView)
        tableView.delegate = dataManager.delegateForTableView(tableView: tableView, withBaseDelegate: nil)
        tableView.reloadData()
    }

    func showDetailFor(object: Any) {
        if !isAssemblySet {
            DetailAssembly.setupAssemblyFor(vc: self)
            isAssemblySet = true
        }
        output?.showDetailFor(object: object)
    }

    func setActivityIndicator(toShow: Bool) {
        if toShow {
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
        } else {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }

    func dataSourceUpdated() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    func setupFooterView() {
        let indicator = UIActivityIndicatorView.init(style: .gray)
        if #available(iOS 13.0, *) {
            indicator.style = .large
        }
        indicator.startAnimating()
        indicator.frame = CGRect.init(x: 0, y: 0, width: tableView.bounds.width, height: 70)
        tableView.tableFooterView = indicator
    }

    func hideFooterView() {
        tableView.tableFooterView = nil
    }
}

extension DetailViewController: ItemActionDelegate {
    func didClickOn(item: CellObject) {
        output?.didClickOn(item: item)
    }
}
