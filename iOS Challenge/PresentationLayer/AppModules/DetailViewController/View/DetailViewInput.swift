//
//  DetailViewInput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation

protocol DetailViewInput: class {
    func setupInitialState()
    func setActivityIndicator(toShow: Bool)

    func configureTableFor(object: DetailCellObject)
    func dataSourceUpdated()

    func setupFooterView()
    func hideFooterView()

    // Note: Passed before view gets initialized
    // Only used for data flow from one module to another
    func showDetailFor(object: Any)
}
