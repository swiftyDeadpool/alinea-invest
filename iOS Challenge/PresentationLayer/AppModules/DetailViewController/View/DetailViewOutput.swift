//
//  DetailViewOutput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

protocol DetailViewOutput: class {
    var viewer: DetailViewInput? { get set }
    var interactor: DetailInteractorInput? { get set }
    var router: DetailRouterInput? { get set }

    func viewDidLoad()
    func viewWillAppear()
    func viewWillDisappear()
    func showDetailFor(object: Any)
    func didClickOn(item: CellObject)
    
    func getDetailObject() -> DetailCellObject
}
