//
//  ExplorePresenter.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import Foundation

class ExplorePresenter: ExploreModuleInput {

    weak var viewer: ExploreViewInput?
    var interactor: ExploreInteractorInput?
    var router: ExploreRouterInput?

    // MARK: - ExploreModuleInput

}

// MARK: - ExploreViewOutput

extension ExplorePresenter: ExploreViewOutput {

    func viewIsReady() {
        viewer?.setupInitialState()
        viewer?.setupCollectionView()
        viewer?.showActivityIndicator()

        interactor?.fetchContents()
    }

    func dataUpdatedNotification() {
        interactor?.fetchContents()
    }
}

// MARK: - ExploreInteractorOutput

extension ExplorePresenter: ExploreInteractorOutput {

    func configureView(contents: [Any]) {
        viewer?.hideActivityIndicator()
        viewer?.configureView(contents: contents)
    }
}
