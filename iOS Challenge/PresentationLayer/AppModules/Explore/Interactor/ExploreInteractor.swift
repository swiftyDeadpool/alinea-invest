//
//  ExploreInteractor.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Sensibol Audio Technologies Pvt. Ltd.. All rights reserved.
//

import Foundation

class ExploreInteractor: ExploreInteractorInput {

	var output: ExploreInteractorOutput?

    // MARK: - ExploreInteractorInput

    func fetchContents() {
        DataService.shared.getContent(completion: { status, error, items in
            if status {
                self.output?.configureView(contents: items)
            } else {
                // Show error
            }
        })
    }

    // MARK: - Private Methods

}
