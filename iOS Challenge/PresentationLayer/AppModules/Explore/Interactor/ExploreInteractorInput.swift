//
//  ExploreInteractorInput.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import Foundation

protocol ExploreInteractorInput {
	var output: ExploreInteractorOutput? { get set }

    func fetchContents()
}
