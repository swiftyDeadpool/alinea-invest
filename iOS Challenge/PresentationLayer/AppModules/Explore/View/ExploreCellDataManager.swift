//
//  ExploreCellDataManager.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Nimbus
import UIKit

protocol ExploreCellDataManagerDelegate: class {

}

class ExploreCellDataManager: NSObject, UITableViewDelegate {
    weak var delegate: ExploreCellDataManagerDelegate?

    var collectionViewModel: NIMutableCollectionViewModel?
    var collectionViewActions: NICollectionViewActions?
    var cellObjects: [Any] = []

    func configureDataManagerWith(item: Any) {
        self.cellObjects = [Any]()

        let obj = item as! ExploreContentCellObject

        switch obj.type {
        case .category:
            for (index, item) in obj.items.enumerated() {
                let cell = ExploreItemCellObject.objectWith(content: item)
                if index == obj.items.count - 1 {
                    cell.isLast = true
                }
                cellObjects.append(cell)
            }
        case .themes:
            for item in obj.items {
                let cell = ExploreThemeCellObject.objectWith(content: item)
                cellObjects.append(cell)
            }
        case .trending:
            for (index, item) in obj.items.enumerated() {
                let cell = ExploreTrendCellObject.objectWith(content: item)

                if (item as! TrendingObject).isObjectValid() {
                    if index == obj.items.count - 1 {
                        cell.isLast = true
                    }
                    cellObjects.append(cell)
                }
            }
        default:
            break
        }

        if self.collectionViewModel?.sections.count ?? 0 > 0 {
            _ = self.collectionViewModel?.removeSection(at: 0)
        }

        _ = self.collectionViewModel?.addSection(withTitle: "Top Gainers")
        _ = self.collectionViewModel?.addObjects(from: self.cellObjects)
    }

    func dataSourceFor(collectionView: UICollectionView) -> UICollectionViewDataSource {
        self.collectionViewModel = NIMutableCollectionViewModel.init(sectionedArray: self.cellObjects, delegate: NICollectionViewCellFactory.self as? NICollectionViewModelDelegate)
        return self.collectionViewModel!
    }

    func delegateFor(collectionView: UICollectionView) -> UICollectionViewDelegate {
        if self.collectionViewActions == nil {
            self.setupCollectionActions()
        }
        return self
    }

    // MARK: - Private methods

    func setupCollectionActions () {
        self.collectionViewActions =  NICollectionViewActions.init(target: self)

        let cellActionBlock: NIActionBlock = {
            _, _, _ in
            return true
        }
        self.collectionViewActions?.attach(to: ExploreItemCellObject.self, tap: cellActionBlock)
    }
}

extension ExploreCellDataManager: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        return (self.collectionViewActions?.collectionView(collectionView, didSelectItemAt: indexPath))!
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {

    }
}
