//
//  ExploreTrendCellObject.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation
import Nimbus

class ExploreTrendCellObject: NSObject, NICollectionViewNibCellObject {
    var content: TrendingObject
    var isLast: Bool = false

    public init(content: Any) {
        self.content = content as! TrendingObject
    }

    class func objectWith(content: Any) -> ExploreTrendCellObject {
        return ExploreTrendCellObject.init(content: content)
    }

    @objc func collectionViewCellNib() -> UINib! {
        return UINib.init(nibName: NSStringFromClass(ExploreTrendCollectionViewCell.self).components(separatedBy: ".").last!, bundle: Bundle.main)
    }

    @objc func cellNibClass() -> AnyClass! {
        return ExploreTrendCollectionViewCell.self
    }
}
