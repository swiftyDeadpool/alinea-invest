//
//  ExploreItemCellObject.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation
import Nimbus

class ExploreItemCellObject: NSObject, NICollectionViewNibCellObject {
    var content: CellObject
    var isLast: Bool = false

    public init(content: Any) {
        self.content = content as! CellObject
    }

    class func objectWith(content: Any) -> ExploreItemCellObject {
        return ExploreItemCellObject.init(content: content)
    }

    @objc func collectionViewCellNib() -> UINib! {
        return UINib.init(nibName: NSStringFromClass(ExploreItemCollectionViewCell.self).components(separatedBy: ".").last!, bundle: Bundle.main)
    }

    @objc func cellNibClass() -> AnyClass! {
        return ExploreItemCollectionViewCell.self
    }
}
