//
//  ExploreCellObject.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import Foundation
import Nimbus

class ExploreCellObject: NSObject, NICollectionViewNibCellObject {
    var content: Any!

    public init(content: Any) {
        self.content = content
    }

    class func objectWith(content: Any) -> ExploreCellObject {
        return ExploreCellObject.init(content: content)
    }

    @objc func collectionViewCellNib() -> UINib! {
        return UINib.init(nibName: NSStringFromClass(ExploreCollectionViewCell.self).components(separatedBy: ".").last!, bundle: Bundle.main)
    }

    @objc func cellNibClass() -> AnyClass! {
        return ExploreCollectionViewCell.self
    }
}
