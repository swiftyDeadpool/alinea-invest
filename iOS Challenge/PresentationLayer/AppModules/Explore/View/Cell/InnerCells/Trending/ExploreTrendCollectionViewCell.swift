//
//  ExploreTrendCollectionViewCell.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit
import Nimbus

class ExploreTrendCollectionViewCell: UICollectionViewCell, NICollectionViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var labelContainerView: UIView!
    @IBOutlet weak var separaterView: UIView!
    @IBOutlet weak var imgView: UIImageView!

    @IBOutlet weak var separaterHeight: NSLayoutConstraint!

    override func layoutSubviews() {
        setupCornerRadius()
        super.layoutSubviews()
    }

    func shouldUpdate(with object: Any!) -> Bool {
        guard let data = object as? ExploreTrendCellObject else { return false }

        titleLbl?.text = data.content.title
        subTitleLbl?.text = (data.content.subtitle ?? "").uppercased()
        imgView.setImageWith(data.content.imgUrl, placeholderImage: "home")

        switch data.content.getGainType() {
        case .bullish:
            labelContainerView.backgroundColor = UIColor(red: 0.45, green: 0.85, blue: 0.71, alpha: 1.00)
            valueLbl?.text = "+\(data.content.getChangeValue())%"
        case .bearish:
            labelContainerView.backgroundColor = UIColor(red: 0.93, green: 0.45, blue: 0.44, alpha: 1.00)
            valueLbl?.text = "-\(data.content.getChangeValue())%"
        default:
            labelContainerView.backgroundColor = .lightGray
            valueLbl?.text = "\(data.content.getChangeValue())%"
        }

        if data.isLast {
            separaterHeight.constant = 5
            separaterView.backgroundColor = UIColor(red: 0.96, green: 0.97, blue: 0.97, alpha: 1.00)
        } else {
            separaterHeight.constant = 0.5
            separaterView.backgroundColor = .lightGray
        }
        setupCornerRadius()
        return true
    }

    private func setupCornerRadius() {
        labelContainerView.clipsToBounds = true
        labelContainerView.layer.cornerRadius = labelContainerView.bounds.size.height / 2
    }
}
