//
//  ExploreItemCollectionViewCell.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit
import Nimbus

class ExploreItemCollectionViewCell: UICollectionViewCell, NICollectionViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var separaterView: UIView!
    @IBOutlet weak var imgView: UIImageView!

    override func layoutSubviews() {
        setupCornerRadius()
        super.layoutSubviews()
    }

    func shouldUpdate(with object: Any!) -> Bool {
        guard let data = object as? ExploreItemCellObject else { return false }

        titleLbl?.text = data.content.title
        containerView.backgroundColor = data.content.backgroundColor
        imgView.image = UIImage.init(named: data.content.imgUrl ?? "home")?.withRenderingMode(.alwaysTemplate)
        imgView.tintColor = .white
        separaterView.isHidden = data.isLast
        separaterView.backgroundColor = UIColor.lightGray
        setupCornerRadius()
        return true
    }

    func setupCornerRadius() {
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = containerView.bounds.height / 2
    }
}
