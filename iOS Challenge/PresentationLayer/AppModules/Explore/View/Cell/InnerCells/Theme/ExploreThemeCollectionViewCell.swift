//
//  ExploreThemeCollectionViewCell.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit
import Nimbus

class ExploreThemeCollectionViewCell: UICollectionViewCell, NICollectionViewCell {

    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var imgView: UIImageView!

    override func layoutSubviews() {
        setupCornerRadius()
        super.layoutSubviews()
    }

    func shouldUpdate(with object: Any!) -> Bool {
        guard let data = object as? ExploreThemeCellObject else { return false }

        titleLbl?.text = data.content.title
        imgView.image = UIImage.init(named: data.content.imgUrl ?? "home")
        //UIColor(red: 0.96, green: 0.97, blue: 0.97, alpha: 1.00)
        setupCornerRadius()
        return true
    }

    func setupCornerRadius() {
        containerView.clipsToBounds = true
        containerView.layer.cornerRadius = 10

        containerView.layer.borderWidth = 0.5
        containerView.layer.borderColor = UIColor.lightGray.cgColor
    }
}
