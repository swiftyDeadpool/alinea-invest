//
//  ExploreCollectionViewCell.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import UIKit
import Nimbus

class ExploreCollectionViewCell: UICollectionViewCell, NICollectionViewCell {
    var cellContent: Any!
    var collectionView: UICollectionView!
    var dataManager: ExploreCellDataManager!
    var loader: UIActivityIndicatorView!

    override func didMoveToSuperview() {
        super.didMoveToSuperview()
    }

    func shouldUpdate(with object: Any!) -> Bool {
        guard let option = object as? ExploreCellObject else { return false }
        cellContent = option.content

        if collectionView == nil {
            let viewSize = self.bounds.size
            let layout = ExploreItemFlowLayout()
            layout.cellType = (cellContent as! ExploreContentCellObject).type
            
            collectionView = UICollectionView.init(frame: CGRect.init(x: 0, y: 0, width: viewSize.width, height: viewSize.height), collectionViewLayout: layout)
            collectionView.backgroundColor = .white
            self.addSubview(collectionView)

            dataManager = ExploreCellDataManager()
            setupLoader()
        }

        dataManager.configureDataManagerWith(item: cellContent as Any)
        collectionView.dataSource = dataManager.dataSourceFor(collectionView: collectionView)
        collectionView.delegate = dataManager.delegateFor(collectionView: collectionView)
        collectionView.reloadData()

        return true
    }

    private func setupLoader() {
/*
        if loader == nil {
            loader = UIActivityIndicatorView.init(style: .gray)
            if #available(iOS 13, *) {
                loader.style = .large
            }
            loader.center = self.center
            loader.color = .darkGray
            loader.hidesWhenStopped = true
            self.addSubview(loader)
        }

        if (cellContent as! ExploreContentCellObject).items.isEmpty {
            loader.isHidden = false
            loader.startAnimating()
            self.bringSubviewToFront(loader)
        } else {
            loader.stopAnimating()
            loader.isHidden = true
        }
 */
    }
}

