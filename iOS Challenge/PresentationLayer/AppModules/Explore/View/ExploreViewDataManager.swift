//
//  ExploreViewDataManager.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import Nimbus
import UIKit

protocol ExploreViewDataManagerDelegate: class {
    func collectionScrolledTo(page: Int)
}

class ExploreViewDataManager: NSObject, UITableViewDelegate {
    weak var view: ExploreViewController?
    weak var delegate: ExploreViewDataManagerDelegate?

    var collectionViewModel: NIMutableCollectionViewModel?
    var collectionViewActions: NICollectionViewActions?
    var cellObjects: [Any] = []

    func configureDataManagerWithItem(items: [Any]) {
        self.cellObjects = [Any]()

        for item in items {
            let object = ExploreCellObject.objectWith(content: item)
            cellObjects.append(object)
        }

        _ = self.collectionViewModel?.removeSection(at: 0)
        _ = self.collectionViewModel?.addSection(withTitle: "")
        _ = self.collectionViewModel?.addObjects(from: self.cellObjects)
    }

    func dataSourceFor(collectionView: UICollectionView) -> UICollectionViewDataSource {
        self.collectionViewModel = NIMutableCollectionViewModel.init(sectionedArray: self.cellObjects, delegate: NICollectionViewCellFactory.self as?NICollectionViewModelDelegate)
        return self.collectionViewModel!
    }

    func delegateFor(collectionView: UICollectionView) -> UICollectionViewDelegate {
        if self.collectionViewActions == nil {
            self.setupCollectionActions()
        }
        return self
    }

    // MARK: - Private methods

    func setupCollectionActions () {
        self.collectionViewActions =  NICollectionViewActions.init(target: self)

        let cellActionBlock: NIActionBlock = {
            object, controller, indexPath in
            return true
        }
        self.collectionViewActions?.attach(to: ExploreCellObject.self, tap: cellActionBlock)
    }
}

extension ExploreViewDataManager: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        return (self.collectionViewActions?.collectionView(collectionView, didSelectItemAt: indexPath))!
    }

    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let xOffset = scrollView.contentOffset.x
        let width = scrollView.bounds.size.width

        let currentPage = Int(ceil(xOffset/width))
        guard currentPage >= 0 else { return }
        delegate?.collectionScrolledTo(page: currentPage)
    }
}
