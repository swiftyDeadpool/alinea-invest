//
//  ExploreViewController.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import UIKit

class ExploreViewController: UIViewController {
    var collectionView: UICollectionView!
    var output: ExploreViewOutput!
    var dataDisplayManager: ExploreViewDataManager!

    private var viewTabs: ViewTabs!
    private var isAssemblySet: Bool = false
    private var loader: UIActivityIndicatorView = UIActivityIndicatorView.init(style: .gray)

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        if !isAssemblySet {
            ExploreModuleAssembly.setupAssemblyFor(vc: self)
            isAssemblySet = true
        }
        output?.viewIsReady()
    }
}

// MARK: - ExploreViewInput

extension ExploreViewController: ExploreViewInput {

    func setupInitialState() {
        view.backgroundColor = .white

        NotificationCenter.default.addObserver(self, selector: #selector(dataUpdated), name: Notification.Name.init("API.dataUpdate"), object: nil)

        setupNavBar()
        setupBarButtons()
        setupCollectionHeaderView()
        setupLoader()
    }

    func setupCollectionView() {
        let viewSize = view.bounds.size
        let layout = ExploreFlowLayout()
        collectionView = UICollectionView.init(frame: CGRect.init(x: 0, y: topBarHeight + Constants.viewTabsHeight, width: viewSize.width, height: viewSize.height - topBarHeight - Constants.viewTabsHeight), collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        view.addSubview(collectionView)
    }

    func configureView(contents: [Any]) {
        dataDisplayManager.delegate = self
        dataDisplayManager.configureDataManagerWithItem(items: contents)
        collectionView.dataSource = dataDisplayManager.dataSourceFor(collectionView: collectionView)
        collectionView.delegate = dataDisplayManager.delegateFor(collectionView: collectionView)
        // For demo purpose
        // For production, Scroll collectionView to index based on tabs
        viewTabs.update(index: 0)

        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.collectionView.setNeedsLayout()
        }
    }

    func showActivityIndicator() {
        view.bringSubviewToFront(loader)
        loader.isHidden = false
        loader.startAnimating()
    }

    func hideActivityIndicator() {
        loader.isHidden = true
        loader.stopAnimating()
    }

    private func setupCollectionHeaderView() {
        viewTabs = ViewTabs.init(frame: CGRect.init(x: 0, y: topBarHeight, width: view.bounds.size.width, height: Constants.viewTabsHeight))
        viewTabs.delegate = self
        view.addSubview(viewTabs)
    }

    private func setupLoader() {
        loader = UIActivityIndicatorView.init(style: .gray)
        if #available(iOS 13, *) {
            loader.style = .large
        }
        loader.center = view.center
        loader.color = .darkGray
        loader.hidesWhenStopped = true
        view.addSubview(loader)
        view.bringSubviewToFront(loader)
    }

    private func setupNavBar() {
        let navBar = self.navigationController!.navigationBar
        navBar.backgroundColor = .clear
        self.navigationItem.title = "Explore"

        if #available(iOS 13.0, *) {
            self.navigationController?.navigationBar.barTintColor = .white
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBar.scrollEdgeAppearance = navBarAppearance
            navBarAppearance.shadowColor = .clear
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.black]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
            navBarAppearance.backgroundColor = .white
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        } else {
            navBar.barTintColor = UIColor.clear
            navBar.isTranslucent = false
            navBar.setBackgroundImage(UIImage(), for: .default)
            navBar.shadowImage = UIImage()
        }
    }

    private func setupBarButtons() {
        let leftBarButton = UIBarButtonItem.init(image: UIImage.init(named: "menu"), style: .plain, target: nil, action: nil)
        let rightBarButton = UIBarButtonItem.init(image: UIImage.init(named: "bell"), style: .plain, target: nil, action: nil)
        leftBarButton.tintColor = .black
        rightBarButton.tintColor = .black

        self.navigationItem.leftBarButtonItem = leftBarButton
        self.navigationItem.rightBarButtonItem = rightBarButton
    }

    @objc private func dataUpdated() {
        output?.dataUpdatedNotification()
    }
}

extension ExploreViewController: ExploreViewDataManagerDelegate {
    func collectionScrolledTo(page: Int) {
        viewTabs.update(index: page)
    }
}

extension ExploreViewController: ViewTabDelegate {
    func tabIndexUpdated(_ index: Int) {
        self.collectionView.scrollToItem(at: IndexPath.init(item: index, section: 0), at: .centeredHorizontally, animated: true)
    }
}
