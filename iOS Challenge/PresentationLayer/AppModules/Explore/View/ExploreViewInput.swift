//
//  ExploreViewInput.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import Foundation


protocol ExploreViewInput: class {

    /**
        @author Unmesh
        Setup initial state of the view
    */

    func setupInitialState()
    func setupCollectionView()
    func configureView(contents: [Any])

    func showActivityIndicator()
    func hideActivityIndicator()
}
