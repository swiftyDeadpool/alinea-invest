//
//  ExploreItemFlowLayout.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class ExploreItemFlowLayout: UICollectionViewFlowLayout {
    var cellType: ContentType = .category

    override func prepare() {
        super.prepare()

        self.sectionInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.collectionView!.contentInset = UIEdgeInsets(top: 15.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.minimumLineSpacing = 0
        self.minimumInteritemSpacing = 0

        let size = collectionView!.bounds.size

        switch cellType {
        case .category:
            self.itemSize = CGSize.init(width: size.width, height: 100)
        case .themes:
            let width = (size.width - 40) / 2 - 10
            self.itemSize = CGSize.init(width: width, height: width)
            self.sectionInset = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
            self.minimumLineSpacing = 20
            self.minimumInteritemSpacing = 20
            self.collectionView!.contentInset = UIEdgeInsets(top: 20.0, left: 0.0, bottom: 0.0, right: 0.0)
        case .trending:
            self.itemSize = CGSize.init(width: size.width, height: 100)
            self.collectionView!.contentInset = UIEdgeInsets(top: 5.0, left: 0.0, bottom: 0.0, right: 0.0)
        case .header:
            self.itemSize = CGSize.init(width: size.width, height: 60)
        }

        self.scrollDirection = .vertical

        Logger.log(type: .viewSize, "Category: \(cellType), Size: \(self.itemSize)")
    }
}
