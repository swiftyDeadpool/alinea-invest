//
//  ExploreFlowLayout.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 15/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class ExploreFlowLayout: UICollectionViewFlowLayout {
    override func prepare() {
        super.prepare()

        let size = collectionView!.bounds.size
        self.itemSize = CGSize.init(width: size.width, height: size.height)
        self.scrollDirection = .horizontal
        self.sectionInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        self.minimumLineSpacing = 0
        self.minimumInteritemSpacing = 0
    }

    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint, withScrollingVelocity velocity: CGPoint) -> CGPoint {
        let width = collectionView!.bounds.size.width
        let rawPageValue: CGFloat = (self.collectionView!.contentOffset.x / width)
        let currentPage: CGFloat = (velocity.x > 0.0) ? floor(rawPageValue): ceil(rawPageValue)
        let nextPage: CGFloat = (velocity.x > 0.0) ? ceil(rawPageValue): floor(rawPageValue)

        let pannedLessThanAPage: Bool = abs(1 + currentPage - rawPageValue) > 0.5
        let flicked: Bool = abs(velocity.x) >  0.5
        var offset: CGPoint = proposedContentOffset

        if pannedLessThanAPage && flicked {
            offset.x = nextPage * width
        } else {
            offset.x = round(rawPageValue) * width
        }

        return offset
    }
}
