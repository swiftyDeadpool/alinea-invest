//
//  ExploreViewOutput.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import Foundation


protocol ExploreViewOutput {
	var viewer: ExploreViewInput? { get set }
    var interactor: ExploreInteractorInput? { get set }
    var router: ExploreRouterInput? { get set }

    /**
        @author Unmesh
        Notify presenter that view is ready
    */

    func viewIsReady()
    func dataUpdatedNotification()
}
