//
//  ExploreAssembly.swift
//  Gaona
//
//  Created by Unmesh on 13/08/2020.
//  Copyright © 2020 Unmesh. All rights reserved.
//

import UIKit

class ExploreModuleAssembly {
    class func setupAssemblyFor(vc: ExploreViewController) {
        let presenter: ExploreViewOutput & ExploreInteractorOutput = ExplorePresenter()

        vc.output = presenter
        vc.output?.router = ExploreRouter()
        vc.output?.viewer = vc
        vc.output?.interactor = ExploreInteractor()
        vc.output?.interactor?.output = presenter

        vc.dataDisplayManager = ExploreViewDataManager()
        vc.dataDisplayManager.view = vc
    }
}
