//
//  AppInitViewOutput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

protocol AppInitViewOutput: class {
    var viewer: AppInitViewInput? { get set }
    var interactor: AppInitInteractorInput? { get set }
    var router: AppInitRouterInput? { get set }

    func viewDidLoad()
    func viewWillAppear()
}
