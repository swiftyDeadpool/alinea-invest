//
//  AppInitViewController.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class AppInitViewController: UIViewController, AppInitViewInput {
    var output: AppInitViewOutput?

    @UsesAutoLayout
    var loader = UIActivityIndicatorView.init(style: .gray)

    // MARK: -  LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        AppInitAssembly.setupAssemblyFor(vc: self)
        output?.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output?.viewWillAppear()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    // MARK: - AppInitViewInput

    func setupInitialState() {
        view.backgroundColor = .white

        let imgView = UIImageView.init(image: UIImage.init(named: "home"))
        imgView.center = CGPoint.init(x: view.center.x, y: view.center.y - (imgView.bounds.height / 2))
        view.addSubview(imgView)

        let lbl = UILabel()
        lbl.translatesAutoresizingMaskIntoConstraints = false
        lbl.text = "iOS Coding Challenge"
        lbl.font = UIFont.systemFont(ofSize: 20, weight: .semibold)
        lbl.textColor = .black
        lbl.textAlignment = .center
        view.addSubview(lbl)

        let lblConstraint = [
            lbl.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 20),
            lbl.centerXAnchor.constraint(equalTo: imgView.centerXAnchor, constant: 0)
        ]

        NSLayoutConstraint.activate(lblConstraint)

        if #available(iOS 13.0, *) {
            loader.style = .large
        }
        loader.startAnimating()
        view.addSubview(loader)

        let loaderConstraint = [
            loader.topAnchor.constraint(equalTo: lbl.bottomAnchor, constant: 20),
            loader.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: 0)
        ]
        NSLayoutConstraint.activate(loaderConstraint)
    }

    func stopActivityIndicator() {
        loader.stopAnimating()
    }
}
