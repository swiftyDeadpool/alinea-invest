//
//  AppInitPresenter.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class AppInitPresenter: AppInitViewOutput, AppInitInteractorOutput {

    weak var viewer: AppInitViewInput?
    var interactor: AppInitInteractorInput?
    var router: AppInitRouterInput?

    // MARK: - AppInitViewOutput

    func viewDidLoad() {
        viewer?.setupInitialState()
    }

    func viewWillAppear() {
        interactor?.checkAppConfig()
    }

    // MARK: - AppInitInteractorOutput

    func appConfig(status: Bool) {
        if status, viewer != nil {
            router?.loadAppHome(vc: viewer! as! AppInitViewController)
        }
    }
}
