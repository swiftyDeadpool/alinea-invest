//
//  AppInitAssembly.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class AppInitAssembly {
    class func setupAssemblyFor(vc: AppInitViewController) {
        let presenter: AppInitViewOutput & AppInitInteractorOutput = AppInitPresenter()

        vc.output = presenter
        vc.output?.router = AppInitRouter()
        vc.output?.viewer = vc
        vc.output?.interactor = AppInitInteractor()
        vc.output?.interactor?.output = presenter
    }
}
