//
//  AppInitInteractor.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

class AppInitInteractor: AppInitInteractorInput {
    var output: AppInitInteractorOutput?

    // MARK: -  AppInitInteractorInput

    func checkAppConfig() {
        // Provision for checking app expiry or other stuff
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.output?.appConfig(status: true)
        })
    }

    // MARK: -  PrivateMethods
}
