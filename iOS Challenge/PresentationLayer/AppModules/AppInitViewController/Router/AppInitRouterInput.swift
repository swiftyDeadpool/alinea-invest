//
//  AppInitRouterInput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

protocol AppInitRouterInput: class {
    func loadAppHome(vc: AppInitViewController)
}
