//
//  AppInitRouter.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class AppInitRouter: AppInitRouterInput {
    func loadAppHome(vc: AppInitViewController) {
        let destinationVC = AppHomeViewController()
        destinationVC.modalPresentationStyle = .fullScreen
        vc.present(destinationVC, animated: false, completion: nil)
    }
}
