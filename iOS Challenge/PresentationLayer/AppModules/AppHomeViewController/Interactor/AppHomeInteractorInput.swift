//
//  AppHomeInteractorInput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 13/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

protocol AppHomeInteractorInput: class {
    var output: AppHomeInteractorOutput? { get set }

    func initSetup()
}
