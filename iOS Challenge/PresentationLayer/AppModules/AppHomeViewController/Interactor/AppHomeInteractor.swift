//
//  AppHomeInteractor.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 13/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import Foundation

class AppHomeInteractor: AppHomeInteractorInput {
    var output: AppHomeInteractorOutput?

    func initSetup() {
        DataService.shared.getSymbolList() { _, _, _ in
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "API.dataUpdate"), object: nil, userInfo: nil)
        }
    }
}
