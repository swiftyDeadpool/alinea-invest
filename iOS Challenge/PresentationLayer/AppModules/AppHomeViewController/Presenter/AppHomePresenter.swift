//
//  AppHomePresenter.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 13/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class AppHomePresenter: AppHomeViewOutput, AppHomeInteractorOutput {
    weak var viewer: AppHomeViewInput?
    var interactor: AppHomeInteractorInput?
    var router: AppHomeRouterInput?

    // MARK: -  AppHomeViewOutput

    func viewDidLoad() {
        viewer?.setupInitialState()
        viewer?.setupTabBar()
        interactor?.initSetup()
    }

    func viewWillAppear() {

    }
}
