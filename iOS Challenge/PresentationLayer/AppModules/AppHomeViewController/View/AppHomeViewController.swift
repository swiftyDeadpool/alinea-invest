//
//  AppHomeViewController.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 13/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class AppHomeViewController: UITabBarController, AppHomeViewInput {
    var output: AppHomeViewOutput?

    // This would allow it be moduler in case some specific controller needs to be explosed for framework
    private var exploreNC: UINavigationController!

    private var homeVC: ViewController!
    private var exploreVC: ExploreViewController!
    private var trendVC: ViewController!
    private var userVC: ViewController!
    private var extraVC: ViewController!

    // MARK: -  LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        AppHomeAssembly.setupAssemblyFor(vc: self)
        output?.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        output?.viewWillAppear()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

    }

    // MARK: -  AppHomeViewInput

    func setupInitialState() {
        view.backgroundColor = .white
    }

    func setupTabBar() {
        initRootVCs()

        self.viewControllers = [homeVC, exploreNC, trendVC, userVC, extraVC]
        selectedIndex = 1 // Making expore controller default
        tabBar.barTintColor = .white

        setupTabIcons()
    }

    func initRootVCs() {
        homeVC = ViewController()
        exploreVC = ExploreViewController()
        trendVC = ViewController()
        userVC = ViewController()
        extraVC = ViewController()

        exploreNC = UINavigationController.init(rootViewController: exploreVC)
    }

    func setupTabIcons() {
        let tabIcons = ["homeTab", "search", "chart", "user", "bulb"]
        for (index, controller) in (self.viewControllers ?? []).enumerated() {
            let img = UIImage.init(named: tabIcons[index])?.imageResize(sizeChange: CGSize.init(width: 30, height: 30))
            let tab = UITabBarItem.init(title: "", image: img, tag: index)
            controller.tabBarItem = tab
        }
    }
}
