//
//  AppHomeAssembly.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 13/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

class AppHomeAssembly {
    class func setupAssemblyFor(vc: AppHomeViewController) {
        let presenter: AppHomeViewOutput & AppHomeInteractorOutput = AppHomePresenter()

        vc.output = presenter
        vc.output?.router = AppHomeRouter()
        vc.output?.viewer = vc
        vc.output?.interactor = AppHomeInteractor()
        vc.output?.interactor?.output = presenter
    }
}
