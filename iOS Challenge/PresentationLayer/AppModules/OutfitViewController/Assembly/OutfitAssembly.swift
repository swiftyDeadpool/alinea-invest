//
//  OutfitAssembly.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class OutfitAssembly {
    class func setupAssemblyFor(vc: OutfitViewController) {
        let presenter: OutfitViewOutput & OutfitInteractorOutput = OutfitPresenter()

        vc.output = presenter
        vc.output?.router = OutfitRouter()
        vc.output?.viewer = vc
        vc.output?.interactor = OutfitInteractor()
        vc.output?.interactor?.output = presenter
    }
}
