//
//  OutfitInteractorInput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation

protocol OutfitInteractorInput: class {
    var output: OutfitInteractorOutput? { get set }

    func fetchContents()
    func getItems() -> [CellObject]
    func getOutfitFor(item: CellObject) -> OutfitObject?
}
