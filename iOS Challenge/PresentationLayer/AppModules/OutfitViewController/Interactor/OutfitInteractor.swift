//
//  OutfitInteractor.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation

class OutfitInteractor: OutfitInteractorInput {
    var output: OutfitInteractorOutput?
    var outfits: [OutfitObject] = []
    var cellObjects: [CellObject] = []

    func fetchContents() {
//        DataService.shared.getOutfits(completion: { (success, error, res) in
//            if success, res != nil, let outfits = res! as? [OutfitObject] {
//                self.outfits = outfits
//                self.updateContents()
//            } else {
//                //TODO: Show error
//            }
//        })
    }

    func getOutfitFor(item: CellObject) -> OutfitObject? {
        guard let outfit = outfits.filter({$0.id! == item.id}).first else {
            return nil
        }
        return outfit
    }

    func getItems() -> [CellObject] {
        return cellObjects
    }

    private func updateContents() {
        cellObjects = []
        for outfit in outfits {
            let item = CellObject.init(id: outfit.id ?? "", type: .outfit, title: outfit.name ?? "-", imgUrl: outfit.full_outfit_picture_url)
            cellObjects.append(item)
        }
        output?.dataSourceUpdated()
    }
}
