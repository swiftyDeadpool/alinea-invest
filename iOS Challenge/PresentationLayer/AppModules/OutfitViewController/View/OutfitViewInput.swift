//
//  OutfitViewInput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import Foundation

protocol OutfitViewInput: class {
    func setupInitialState()
    func updateOutfitCount(to: Int)
    func setActivityIndicator(toShow: Bool)
    func hideNavigationBar()
    func reloadCollection()
}
