//
//  OutfitViewOutput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

protocol OutfitViewOutput: class {
    var viewer: OutfitViewInput? { get set }
    var interactor: OutfitInteractorInput? { get set }
    var router: OutfitRouterInput? { get set }

    func viewDidLoad()
    func viewWillAppear()
    func viewWillDisappear()
    
    func didClickCell(item: CellObject)
    // MARK: -
    func getItems() -> [CellObject]
}
