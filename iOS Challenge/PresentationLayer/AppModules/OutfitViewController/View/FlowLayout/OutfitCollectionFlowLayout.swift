//
//  OutfitCollectionFlowLayout.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class OutfitCollectionFlowLayout: UICollectionViewFlowLayout {

    override func prepare() {
        super.prepare()

        self.itemSize = CGSize.init(width: contentWidth(), height: contentWidth() * 1.4)
        self.scrollDirection = UICollectionView.ScrollDirection.vertical
        self.sectionInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: 0.0, right: 0.0)
        collectionView!.contentInset = UIEdgeInsets.init(top: 5, left: 0, bottom: 10, right: 0)
        self.minimumLineSpacing = 15
        self.minimumInteritemSpacing = 0
    }

    func contentWidth() -> CGFloat {
        return (self.collectionView?.frame.size.width)! / 2 - 7.5
    }
}
