//
//  OutfitViewController.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class OutfitViewController: UIViewController, OutfitViewInput {

    @IBOutlet weak var topPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomPaddingConstraint: NSLayoutConstraint!
    @IBOutlet weak var configView: UIView!
    @IBOutlet weak var outfitCountLbl: UILabel!
    @IBOutlet weak var sortBtn: UIButton!
    @IBOutlet weak var filterBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!

    var output: OutfitViewOutput?
    let activityIndicator = UIActivityIndicatorView(style: .gray)

    // MARK: -  LifeCycle

    override func viewDidLoad() {
        super.viewDidLoad()
        OutfitAssembly.setupAssemblyFor(vc: self)
        output?.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        output?.viewWillDisappear()
    }

    

    // MARK: -  ActionOutlets

    @IBAction func btnSortClicked(_ sender: Any) {

    }

    @IBAction func btnFilterClicked(_ sender: Any) {

    }

    // MARK: - OutfitViewInput

    func setupInitialState() {
        topPaddingConstraint.constant = safeTopPadding + 100
        bottomPaddingConstraint.constant = -safeBottomPadding

        if #available(iOS 13.0, *) {
            activityIndicator.style = .large
        }

        activityIndicator.center = view.center
        activityIndicator.hidesWhenStopped = true
        view.addSubview(activityIndicator)
        view.bringSubviewToFront(activityIndicator)

        collectionView.dataSource = self
        collectionView.delegate = self
        let cellNib = UINib(nibName: "ItemDisplayCell", bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: ItemDisplayCell.reuseId)
    }

    func hideNavigationBar() {
        self.navigationController?.isNavigationBarHidden = true
    }

    func setActivityIndicator(toShow: Bool) {
        if toShow {
            activityIndicator.startAnimating()
            activityIndicator.isHidden = false
        } else {
            activityIndicator.stopAnimating()
            activityIndicator.isHidden = true
        }
    }

    func updateOutfitCount(to: Int) {
        outfitCountLbl.text = "\(to) Outfits"
    }

    func reloadCollection() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
            self.collectionView.setNeedsLayout()
        }
    }
}

// MARK: -  UICollectionViewDataSource

extension OutfitViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return output?.getItems().count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ItemDisplayCell.reuseId, for: indexPath) as! ItemDisplayCell
        if let data = output?.getItems().getElementAt(index: indexPath.item) {
            cell.setData(item: data)
        }
        return cell
    }
}

// MARK: - UICollectionViewDelegate

extension OutfitViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! ItemDisplayCell
        output?.didClickCell(item: cell.data)
    }
}
