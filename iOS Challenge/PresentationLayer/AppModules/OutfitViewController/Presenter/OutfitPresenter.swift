//
//  OutfitPresenter.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class OutfitPresenter: OutfitViewOutput, OutfitInteractorOutput {

    weak var viewer: OutfitViewInput?
    var interactor: OutfitInteractorInput?
    var router: OutfitRouterInput?

    // MARK: - OutfitViewOutput

    func viewDidLoad() {
        viewer?.setupInitialState()
        viewer?.setActivityIndicator(toShow: true)
        interactor?.fetchContents()
    }

    func viewWillAppear() {
        viewer?.hideNavigationBar()
    }

    func viewWillDisappear() {
        viewer?.setActivityIndicator(toShow: false)
    }

    func didClickCell(item: CellObject) {
        guard let outfit: OutfitObject = interactor?.getOutfitFor(item: item) else {
            // Instruct view to show error
            return
        }
        router?.showDetailFor(outfit: outfit, vc: viewer as! OutfitViewController)
    }

    func getItems() -> [CellObject] {
        let items = interactor?.getItems() ?? []
        viewer?.updateOutfitCount(to: items.count)
        return items
    }

    // MARK: - OutfitInteractorOutput

    func dataSourceUpdated() {
        viewer?.setActivityIndicator(toShow: false)
        viewer?.reloadCollection()
    }
}
