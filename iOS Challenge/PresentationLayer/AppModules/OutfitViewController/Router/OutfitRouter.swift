//
//  OutfitRouter.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

class OutfitRouter: OutfitRouterInput {
    var passData: Bool = false
    var dataObject: Any?

    func showDetailFor(outfit: OutfitObject, vc: OutfitViewController) {
        passData = true
        dataObject = outfit
        vc.performSegue(withIdentifier: SegueIdentifier.OutfitToDetailScreen.rawValue, sender: vc)
    }
}
