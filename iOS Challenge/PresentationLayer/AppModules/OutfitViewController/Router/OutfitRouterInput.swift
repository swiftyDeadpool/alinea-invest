//
//  OutfitRouterInput.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Project YX. All rights reserved.
//

import UIKit

protocol OutfitRouterInput: class {
    func showDetailFor(outfit: OutfitObject, vc: OutfitViewController)
}
