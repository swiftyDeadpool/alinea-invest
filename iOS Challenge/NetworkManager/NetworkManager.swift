//
//  NetworkManager.swift
//  iOS Code Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit
import Alamofire

@objc public enum RequestMethod: NSInteger {
    case options = 0, get, head, post, put, patch, delete, trace, connect
}

class NetworkManager {
    
    private static var sharedInstance:NetworkManager?
    static var shared:NetworkManager{
        if sharedInstance != nil {
            return sharedInstance!
        }
        sharedInstance = NetworkManager()
        return sharedInstance!
    }

    func serverRequest(type: RequestMethod, url: String, parameters: [String: String]? = [:], encoding: ParameterEncoding, headers: [String: String]? = [:], completion: @escaping (_ success: Bool, _ error: Error?, _ objects: Any?) -> Void) {

        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }

        Alamofire.request(url, method: getHttpMethod(type), parameters: parameters, encoding: encoding, headers: headers)
            .validate()
            .responseJSON { response in
                self.handleNetworkResponse(response, completionBlock: completion)
        }
    }

    func dispose() {
        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
            sessionDataTask.forEach { $0.cancel() }
            uploadData.forEach { $0.cancel() }
            downloadData.forEach { $0.cancel() }
        }
        NetworkManager.sharedInstance = nil
    }
}

extension NetworkManager {
    fileprivate func handleNetworkResponse(_ response: DataResponse<Any>, completionBlock: @escaping(Bool, Error?, Any?) -> Void) {

        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }

        if response.error == nil {
            guard let json = response.value else {
                completionBlock(false, APIServiceError.jsonError, nil)
                return
            }

            Logger.log(type: .apiResponse, "ResponseJSON: \(json)")
            completionBlock(true, nil, json)

        } else {
            if let error = response.error as? AFError {
                switch error {
                case .invalidURL(let url):
                    Logger.log(type: .apiResponse, "Invalid URL: \(url) - \(error.localizedDescription)")

                case .parameterEncodingFailed(let reason):
                    Logger.log(type: .apiResponse, "Parameter encoding failed: \(error.localizedDescription)")
                    Logger.log(type: .apiResponse, "Failure Reason: \(reason)")

                case .multipartEncodingFailed(let reason):
                    Logger.log(type: .apiResponse, "Multipart encoding failed: \(error.localizedDescription)")
                    Logger.log(type: .apiResponse, "Failure Reason: \(reason)")

                case .responseValidationFailed(let reason):
                    Logger.log(type: .apiResponse, "Response validation failed: \(error.localizedDescription)")
                    Logger.log(type: .apiResponse, "Failure Reason: \(reason)")

                case .responseSerializationFailed(let reason):
                    Logger.log(type: .apiResponse, "Response serialization failed: \(error.localizedDescription)")
                    Logger.log(type: .apiResponse, "Failure Reason: \(reason)")
                }

                Logger.log(type: .apiResponse, "Underlying error: \(error.underlyingError ?? APIServiceError.serverError)")

                completionBlock(false, APIServiceError.serverError,nil)

            } else if let error = response.error as? URLError {
                Logger.log(type: .apiResponse, "URLError: \(error)")
                completionBlock(false, getErrorBy(errorCode: error.code.rawValue), nil)

            } else {
                completionBlock(false, APIServiceError.serverError, nil)
            }
        }
    }

    fileprivate func getErrorBy(errorCode: Int) -> Error {
        var error: Error = APIServiceError.serverError

        switch errorCode {
        case -1001, -1005, -1009: // Request Timed Out, Lost connection to internet, Not connected to internet
            error = APIServiceError.noNetwork
        default:
            error = APIServiceError.serverError
        }
        return error
    }

    private func getHttpMethod(_ method: RequestMethod) -> Alamofire.HTTPMethod {
        switch(method) {
        case .get:
            return .get
        case .post:
            return .post
        case .delete:
            return .delete
        case .head:
            return .head
        case .put:
            return .put
        case .patch:
            return .patch
        case .trace:
            return .trace
        case .connect:
            return .connect
        case .options:
            return .options
        }
    }
}
