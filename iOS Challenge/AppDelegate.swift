//
//  AppDelegate.swift
//  iOS Challenge
//
//  Created by Unmesh Rathod on 11/08/20.
//  Copyright © 2020 Unmesh Rathod. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        window = UIWindow.init(frame: UIScreen.main.bounds)
        window?.rootViewController = AppInitViewController()
        // Show the window
        window?.makeKeyAndVisible()
        return true
    }
}
